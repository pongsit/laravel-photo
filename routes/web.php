<?php

use Illuminate\Support\Facades\Route;
use Pongsit\Photo\Http\Controllers\AlbumController;
use Pongsit\Photo\Http\Controllers\PhotoController;
use Illuminate\Support\Facades\Auth;

Route::get('/photo/show/{type}/{model_name}/{model_slug}/{number}/{size}', [PhotoController::class, 'show'])->name('photo.show');

Route::middleware(['userCan:manage_album'])->group(function () {  
    Route::get('/album/vault/{album:slug}', [AlbumController::class, 'vault'])->name('album.vault');
	Route::get('/album/manage/{album:slug}', [AlbumController::class, 'manage'])->name('album.manage');
    Route::post('/album/update/{album:slug}', [AlbumController::class, 'update'])->name('album.update');
    Route::post('/album/store/{type}/photo/{album:slug}/{overlay?}', [AlbumController::class, 'storePhoto'])->name('album.store.photo');
    Route::post('/album/reorder/{type}/photo/{album:slug}', [AlbumController::class, 'reorderPhoto'])->name('album.reorder.photo');
    Route::post('/album/remove/{type}/photo/{album:slug}', [AlbumController::class, 'removePhoto'])->name('album.remove.photo');
});

// filepond
Route::get('/filepond/api/load/{path}', [AlbumController::class, 'filepondLoad'])->name('album.filepond.load');
Route::delete('/filepond/api/revert', [AlbumController::class, 'revertPhoto'])->name('album.filepond.revert');
// --------