<?php

namespace Pongsit\Photo\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Image;
use Response;
use Sopamo\LaravelFilepond\Filepond;
use Pongsit\Photo\Models\Photo;
use Pongsit\Photo\Models\Album;

class PhotoController extends Controller
{
    public function show($type="public",$model_name,$model_slug,$number,$size){
        if($type!='public'){
            if(!user()->isLogin()){
                return;
            }
            
            // ตรวจสอบการเข้าถึงผ่าน album
            if($model_name == 'album'){
                $album = Album::where('slug',$model_slug)->first();
                if(!(
                    user()->id == $album->user_id ||
                    user()->role_is('tech') || 
                    user()->role_is('admin')
                )){
                    return;
                }
            }
        }

        $path = $type.'/'.$model_name.'/'.$model_slug.'/photo/'.$number.'/'.$size;
        $files = Storage::files($path);
        if(empty($files)){
            return '';
        }

        $file_path = storage_path('app/'.$files[0]);
        if(is_support_webp()){
            if(Storage::exists($path.'/photo.webp')){
                $file_path = storage_path('app/'.$path.'/photo.webp');
            }
        }
        $file = File::get($file_path);
        $type = File::mimeType($file_path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

}