<?php

namespace Pongsit\Photo\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Image;
use Response;
use Sopamo\LaravelFilepond\Filepond;
use Pongsit\Photo\Models\Photo;
use Pongsit\Photo\Models\Album;
use Pongsit\System\Models\Time;

class AlbumController extends Controller
{
    public function manage(Album $album)
    {
        $variables['album'] = $album;
        $album->slotPhotoSync();
        return view('album.manage',$variables);
    }

    public function vault(Album $album)
    {
        $variables['album'] = $album;
        $album->slotPhotoSync('vault');
        return view('album.vault',$variables);
    }

    public function edit(Album $album)
    {
        $variables['album'] = $album;
        $album->slotPhotoSync();
        return view('album.edit',$variables);
    }

    public function storePhoto(Request $request, $type, Album $album, $overlay=false)
    {
        $inputs = $request->all();
        if(empty($inputs['serverId'])){
            return Response(['error'=>'ไม่มี serverId']);
        }

        $serverId = $inputs['serverId'];
        $filepond = app(\Sopamo\LaravelFilepond\Filepond::class);
        $disk = config('filepond.temporary_files_disk');
        $path = $filepond->getPathFromServerId($serverId);

        // $maxfile = 7;
        // $album_path = storage_path('app/'.$type.'/album/'.$album->id.'/photo/');
        // $album_photos = array();

        // if(File::exists($album_path)){
        //     $directories = File::directories($album_path);
        //     if(!empty($directories)){
        //         foreach($directories as $v){
        //             $album_photos[] = basename($v);
        //         }
        //         $count_directory = count($directories);
        //         if($count_directory >= $maxfile){
        //             // if(dirname(dirname($path)) == 'filepond'){
        //                 Storage::deleteDirectory(dirname($path));
        //             // }
        //             return Response([
        //                 'name'=> basename($path),
        //                 'error'=>'จำนวนภาพสูงสุด '.$maxfile.' ภาพเท่านั้นครับ'
        //             ]);
        //         }
        //     }
        // }


        // Get the temporary path using the serverId returned by the upload function in `FilepondController.php`
        // $full_path = Storage::disk($disk)->path($path);
        // $full_path = storage_path('app/'.$path);

        if(!empty($path)){

            // dd(dirname($path));

            $filename = basename($path);
            // $directory = Str::orderedUuid();
            $ext = File::extension($path);

            // for($i=1;$i<=$maxfile;$i++){
            //     if(in_array($i,$album_photos)){ continue; } 
            // $album_photo_id = $album->id.time().rand(1000,9999);
            $album_photo_id = unique();

            if(Storage::exists($type.'/album/'.$album->slug.'/photo/'.$album_photo_id)){
                Storage::deleteDirectory($type.'/album/'.$album->slug.'/photo/'.$album_photo_id);
            }

            $sm_path = $type.'/album/'.$album->slug.'/photo/'.$album_photo_id.'/sm/photo';
            $md_path = $type.'/album/'.$album->slug.'/photo/'.$album_photo_id.'/md/photo';
            $lg_path = $type.'/album/'.$album->slug.'/photo/'.$album_photo_id.'/lg/photo';
            $original_path = $type.'/album/'.$album->slug.'/photo/'.$album_photo_id.'/original/photo';

            Storage::copy($path, $sm_path.'.'.$ext);
            Storage::copy($path, $md_path.'.'.$ext);
            Storage::copy($path, $lg_path.'.'.$ext);
            Storage::copy($path, $original_path.'.'.$ext);

            if($overlay == true){
                $photo = new Photo();
                $photo->overlay(storage_path('app/'.$sm_path.'.'.$ext),storage_path('app/'.$sm_path.'.'.$ext));
                $photo->overlay(storage_path('app/'.$md_path.'.'.$ext),storage_path('app/'.$md_path.'.'.$ext));
                $photo->overlay(storage_path('app/'.$lg_path.'.'.$ext),storage_path('app/'.$lg_path.'.'.$ext));
            }

            // delete tmp file
            // if(dirname(dirname($path)) == 'filepond'){
                Storage::deleteDirectory(dirname($path));
            // }

            $sm = Image::make(storage_path('app/'.$sm_path.'.'.$ext));
            $sm->widen(250);
            $sm->save();
            $sm->encode('webp', 100);
            $sm->save(storage_path('app/'.$sm_path.'.webp'));
            
            $md = Image::make(storage_path('app/'.$md_path.'.'.$ext));
            $md->widen(450);
            $md->save();
            $md->encode('webp', 100);
            $md->save(storage_path('app/'.$md_path.'.webp'));

            $photos = [];
            if(!empty($inputs['photos'])){
                foreach($inputs['photos'] as $k=>$v){
                    $slot = $k+1;
                    if(strtolower($v)==strtolower($filename)){
                        $album->slotPhoto($slot,$album_photo_id,$filename);
                    }
                    if(is_numeric($v)){
                        $album->slotPhoto($slot,$v);
                    }
                }
            }

            return Response([
                'success'=>'ดำเนินการเก็บภาพเรียบร้อย',
                'source' => $album->slug.'-'.$album_photo_id,
                'filename' => $filename,
                'id' => $album_photo_id
            ]);
        }
    }

    public function filepondLoad($path)
    {
        $explodes = explode('-',$path);
        if(empty($explodes[0]) || empty($explodes[1]) || empty($explodes[2]) || empty($explodes[3])){
            return Response([
                'error'=>'ไม่มีไฟล์ดังกล่าว'
            ]);
        }

        $files = Storage::files($explodes[0].'/album/'.$explodes[1].'/photo/'.$explodes[2].'/'.$explodes[3]);

        if(empty($files)){
            return Response([
                'error'=>'ไม่มีไฟล์ดังกล่าว'
            ]);
        }

        $file = File::get(storage_path('app/'.$files[0]));
        $type = File::mimeType(storage_path('app/'.$files[0]));
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        $response->header("Content-Disposition","inline; filename=".$explodes[2]);
        return $response;
    }

    public function reorderPhoto(Request $request ,$type ,Album $album)
    {
        // $request->validate([
        //     'origin' => 'required',
        //     'target' => 'required',
        // ]

        // $photos = $request->photos;
        // $filename_to_ids = array();
        // if(!empty($request->filename_to_ids)){
        //     $filename_to_ids = $request->filename_to_ids;
        // }
        // $album_photos = array();
        // dd($request->photos);

        foreach($request->photos as $k => $v){
            $slot = $k+1;
            if(is_numeric($v)){
                $album->slotPhoto($slot,$v);
            }else{
                $photo = $album->photos()->where('tmp_name',$v)->first();
                if(!empty($photo)){
                    $album->slotPhoto($slot,$photo->name,$v);
                }
            }
        }

        // $inputs = $request->all();
        // Storage::move('public/album/'.$album->id.'/photo/', 'public/album/'.$album->id.'/img-tmp');
        // foreach($inputs['orders'] as $k => $v){
        //     $new_number = $k+1;
        //     Storage::move('public/album/'.$album->id.'/img-tmp/'.$v,'public/album/'.$album->id.'/photo/'.$new_number);
        // }
        // Storage::deleteDirectory('public/album/'.$album->id.'/img-tmp');

        // $origin_folder = $inputs['origin'];
        // $target_folder = $inputs['target']+1;
        // $origin_path = 'public/album/'.$album->id.'/photo/'.$origin_folder;
        // $tmp_path = 'public/album/'.$album->id.'/photo/tmp';
        // Storage::move($origin_path, $tmp_path);
        // $target_path = 'public/album/'.$album->id.'/photo/'.$target_folder;
        // Storage::move($target_path, $origin_path);
        // Storage::move($tmp_path, $target_path);

        return Response([
            'success'=>'ดำเนินการจัดลำดับเรียบร้อย'
        ]);
    }

    public function removePhoto(Request $request,$type , Album $album)
    {
        $request->validate([
            'photo_name' => 'required',
        ]);

        $name = $request->photo_name;

        $photo = $album->photos()->where('tmp_name',$name)->first();
        if(!empty($photo)){
            $name = $photo->name;
        }

        if(Storage::exists($type.'/album/'.$album->slug.'/photo/'.$name)){
            Storage::deleteDirectory($type.'/album/'.$album->slug.'/photo/'.$name);
        }

        $album->slotPhotoDeleteByName($name);

        return Response([
            'success'=>'ดำเนินการเรียบร้อย'
        ]);
    }

    public function revertPhoto(Request $request)
    {
        // we use removePhoto this function doing nothing
    }
}