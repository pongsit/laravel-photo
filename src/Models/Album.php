<?php

namespace Pongsit\Photo\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Album extends Model
{

  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  public function albumable()
  {
      return $this->morphTo();
  }

  public function photos(){
      return $this->morphMany('Pongsit\Photo\Models\Photo', 'photoable');
  }

  // public function __construct(){
  //   parent::__construct();
    
  //   if(empty($this->slug)){
  //     $this->unique = unique();
  //     $this->slug = slug($this,'unique');
  //   }
  // }

  // add photo to the model
    public $class_name = 'album';
    public $photo_shows;
    public $photo_numbers;

    public function getPhotos($type="public"){
        $i = 0;
        foreach($this->photos()->orderBy('slot')->get() as $v){
            $this->photo_shows[$i]['md'] = route('photo.show',[$type,$this->class_name,$this->slug,$v->name,'md']);
            $this->photo_shows[$i]['lg'] = route('photo.show',[$type,$this->class_name,$this->slug,$v->name,'lg']);
            $this->photo_shows[$i]['original'] = route('photo.show',[$type,$this->class_name,$this->slug,$v->name,'original']);
            $this->photo_shows[$i]['sm'] = route('photo.show',[$type,$this->class_name,$this->slug,$v->name,'sm']);
            $i++;
        }
        if(!empty($this->photo_shows)){
            return $this->photo_shows;
        }else{
            return [];
        }
    }

    public function slotPhotoSync($type="public"){
        $path = storage_path('app/'.$type.'/'.$this->class_name.'/'.$this->slug.'/photo/');
        $directories = array();
        if(File::exists($path)){
            $directories = File::directories($path);
        }

        if(!empty($directories)){
            $directory_names = [];
            foreach($directories as $v){
                $d_name = basename($v);
                $directory_names[] = $d_name;
                if(empty($this->getSlotPhoto($d_name))){
                    $this->insertSlotPhoto($d_name);
                }
            }

            if(!empty($this->photos)){
                foreach($this->photos as $v){
                    if(!in_array($v->name,$directory_names)){
                        $v->delete();
                    }
                }
            }
        }else{
            $this->photos()->where('photoable_id',$this->id)->delete();
        }
    }

    public function getSlotPhoto($name){
        if(empty($name)){
            return false;
        }

        $photo = $this->photos()->where('name',$name)->where('photoable_id',$this->id)->first();
        if(!empty($photo)){
            return $photo->slot;
        }

        return false;
    }

    public function insertSlotPhoto($name,$tmp_name=''){
        if(empty($name)){
            return false;
        }

        $max_slot = $this->photos()->where('photoable_id',$this->id)->max('slot');
        $new_slot = $max_slot+1;
        $this->photos()->save(new Photo(['slot'=>$new_slot,'name'=>$name,'tmp_name'=>$tmp_name,'user_id'=>user()->id]));
        return true;
    }

    public function slotPhoto($slot,$name,$tmp_name=''){
        if(empty($slot) || empty($name)){
            return false;
        }

        $this->slotPhotoDeleteByName($name);
        $photo = $this->photos()->where('slot',$slot)->where('photoable_id',$this->id)->first();

        if(!empty($photo)){  
            $this->insertSlotPhoto($photo->name,$photo->tmp_name);
            $photo->name = $name;
            $photo->user_id = user()->id;
            $photo->tmp_name = $tmp_name;
            $photo->save();
        }else{
          $this->photos()->save(new Photo(['slot'=>$slot,'name'=>$name,'tmp_name'=>$tmp_name,'user_id'=>user()->id]));
        }
    }

    public function slotPhotoDelete($slot){
        if(empty($slot)){
            return false;
        }

        $this->photos()->where('slot',$slot)->where('photoable_id',$this->id)->delete();
    }

    public function slotPhotoDeleteByName($name){
        if(empty($name)){
            return false;
        }

        $this->photos()->where('name',$name)->where('photoable_id',$this->id)->delete();
    }

    // public function getFirstPhoto($size='sm'){
    //     if(!empty($this->photos()->orderBy('slot')->first())){
    //         return route($this->class_name.'.show.photo',[$this->slug,$this->photos()->orderBy('slot')->first()->name,$size]);
    //     }
    // }

    public function getFirstPhotos($type="public"){
        $outputs = [];
        $v = $this->photos()->orderBy('slot')->first();
        if(!empty($v)){
          $outputs['md'] = route('photo.show',[$type,$this->class_name,$this->slug,$v->name,'md']);
          $outputs['original'] = route('photo.show',[$type,$this->class_name,$this->slug,$v->name,'original']);
          $outputs['sm'] = route('photo.show',[$type,$this->class_name,$this->slug,$v->name,'sm']);
          return $outputs;
        }
    }

}
