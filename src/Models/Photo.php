<?php

namespace Pongsit\Photo\Models;

use Pongsit\Photo\Models\Album;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{

  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  public function photoable(){
      return $this->morphTo();
  }

  public function album(){
      return $this->belongsTo(Album::class);
  }

  public function overlay($input_path, $output_path){
    // ใส่ overlay logo ไว้ใน public folder
    if(!File::exists(public_path('img/overlay.png'))){
      dd('กรุณาเพิ่ม overlay.png ที่ public/img/overlay.png');
    }

    $background_infos = getimagesize($input_path);

    if($background_infos['mime']=='image/jpeg'){
      $img = imagecreatefromjpeg($input_path);
    }

    if($background_infos['mime']=='image/png'){
      $img = imagecreatefrompng($input_path);
    }

    if(empty($img)){
      return;
    }

    $width = $background_infos[0];
    $height = $background_infos[1];

    $logo = imagecreatefrompng(public_path('img/overlay.png'));

    list($logo_width, $logo_height) = getimagesize(public_path('img/overlay.png'));
    $out = imagecreatetruecolor($width, $height);
    imagecopyresampled($out, $img, 0, 0, 0, 0, $width, $height, $width, $height);
    imagecopyresampled($out, $logo, $width-(ceil(1.5*$logo_width)), $height-(ceil($height/3)+$logo_height), 0, 0, $logo_width, $logo_height, $logo_width, $logo_height);
    if($background_infos['mime']=='image/jpeg'){
      imagejpeg($out, $output_path, 100);
    }
    if($background_infos['mime']=='image/png'){
      imagepng($out, $output_path, 0);
    }
  }
  
}
